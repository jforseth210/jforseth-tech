import os
import platform
import json

import secrets
from flask import *
from flask_simplelogin import get_username, login_required, is_logged_in

from account_management import *
from scripts.prayer import PARISH_DICTIONARY

accounts = Blueprint("accounts", __name__)


# The account page.
@login_required()
@accounts.route("/account/<account>")
def account(account):
    if is_logged_in() and account == get_username():  # .encode("utf-8"):
        if platform.node() == "backup-server-vm":
            flash(
                "The main jforseth.tech server is experiencing issues. Account changes have been suspended."
            )
        account = get_account(get_username())  # .encode("utf-8"))
        groups = account["prayer_groups"].split("|")
        return render_template("accounts/account.html", groups=groups)
    return render_template("errors/403.html"), 403


# The change password form
@accounts.route("/changepw", methods=["GET", "POST"])
def change_password():
    old_password = escape(request.form.get("old_password"))
    new_password = escape(request.form.get("new_password"))
    confirm_new_password = escape(request.form.get("confirm_new_password"))

    current_username = get_username()  # .encode("utf-8")
    current_account = get_account(current_username)

    if platform.node() == "backup-server-vm":
        pass

    elif new_password != confirm_new_password:
        flash("New passwords do not match!", category="Success")

    elif check_password_hash(current_account.get("hashed_password"), old_password):
        update_pw(current_username, new_password)
        flash("Success!", category="success")

    else:
        flash("Old password incorrect.", category="warning")

    return redirect("/account/{}".format(current_username))


# Email change form. Basically just sends an email.
@accounts.route("/change_email", methods=["POST"])
def verify_changed_email():
    email = escape(request.form.get("email"))  # .encode("utf-8")
    email_type = escape(request.form.get("email_type"))

    username = get_username()  # .encode("utf-8")
    token = generate_token(email + username, "email_change")

    if current_app.config["TESTING"]:
        return json.dumps([username, email, email_type, token])

    with open("text/change_email_template.html") as file:
        message = file.read()
    message = message.format(
        username=username, email=email, email_type=email_type.lower(), token=token
    )
    send_email(
        email,
        "Change your " + email_type.lower(),
        message,
        PROJECT_EMAIL,
        PROJECT_PASSWORD,
    )
    flash("We've sent a verification link to that email address.", category="success")
    return redirect("/account/" + username)

# Actually change the email.
# Validation link from the email.


@accounts.route("/change_email/verified")
def change_email_page():
    token = escape(request.args.get("token"))
    username = escape(request.args.get("username"))
    email_type = escape(request.args.get("type"))
    email = escape(request.args.get("email"))

    EMAIL_TYPES = {"Recovery email": "recovery_email",
                   "Prayer email": "prayer_email"}

    if (
        check_token(token, "email_change")
        and get_user_from_token(token, "email_change") == email + username
    ):
        email_type = EMAIL_TYPES.get(email_type)
        change_email(username, email, email_type)
        remove_token(token, "email_change")
        flash("Success!", category="success")
    else:
        flash("That link didn't work, try again.")

    return redirect("/account/" + username)


@accounts.route("/forgot_pw", methods=["GET", "POST"])
def forgot_pw():
    if request.method == "GET":
        return render_template("accounts/forgot.html")
    email = escape(request.form.get("emailInput"))
    username = escape(request.form.get("usernameInput"))

    token = generate_token(username, "password_reset")
    with open("text/password_reset_email_template.html") as file:
        message = file.read()
    message = message.format(token=token)
    if get_account(username)["recovery_email"] == email:
        send_email(
            email,
            "jforseth.tech password reset",
            message,
            PROJECT_EMAIL,
            PROJECT_PASSWORD,
        )
    flash(
        "If that email/username combination exists, an email with reset instructions will be sent.",
        category="success",
    )
    return redirect("/")


@accounts.route("/forgot_pw/reset/<token>", methods=["GET", "POST"])
def reset_password(token):
    if request.method == "GET":
        if check_token(token, "password_reset"):
            return render_template("accounts/reset.html")
        flash("Your reset link is invalid. Try again.")
        return redirect("/forgot_pw/reset/{}".format(token))
    else:
        username = escape(request.form.get("usernameInput"))
        new_password = escape(request.form.get("passwordInput"))
        if not check_token(token, "password_reset"):
            flash("Your reset link is invalid. Try again.")
            return redirect("/forgot_pw/reset/{}".format(token))
        elif get_user_from_token(token, "password_reset") != username:
            flash("Incorrect username.")
            return redirect("/forgot_pw/reset/{}".format(token))
        else:
            update_pw(username, new_password)
            remove_token(token, "password_reset")
            flash("Password reset sucessfully.", category="success")
            return redirect("/login")


@accounts.route("/accountdel", methods=["POST"])
def account_del():
    password = escape(request.form.get("confirm_password"))
    user = get_username()  # .encode("utf-8")
    current_account = get_account(user)
    if check_password_hash(current_account.get("hashed_password"), password):
        delete_account(user)
        flash("Your account has been deleted!", category="success")
        return redirect("/logout")
    else:
        flash("Incorrect password")
