import os
from flask import *
from paramiko import SSHClient, AutoAddPolicy
from SensitiveData import IFTTT_TOKEN
ifttt = Blueprint("ifttt", __name__)
def run_command(commands, hosts=[]):
    host_commands = {
            "jaro": "ssh 10.2.3.2",
            "dino": "ssh 10.2.3.5",
            "lenovosaur": "ssh justin@192.168.1.61",
            "abe": "ssh 10.2.3.4"
    }
    if hosts:
        for host in hosts:
            command = host_commands.get(host, "") + ' "'+";".join(commands)+'"'
            print(command)
            os.system(command)
@ifttt.route("/ifttt/<token>/")
def ifttt_function(token):
    if token != IFTTT_TOKEN:
        print("Invalid Token")
        return ""
    command = request.args.get("command")
    print(command)
    command = command.replace(" ", "")
    command = command.lower()
    print(command)
    if command in ["lightsout", "turnoutthelights", "hitthelights", "killthelights", "turnthelightsoff", "nightmode"]:
        run_command(["xset dpms force off -display :0"], ["jaro", "abe", "dino"])
    if command in ["activatehollywoodmode"]:
        run_command(["export DISPLAY=:0",'nohup alacritty -e "sleep 2;cmatrix"&;disown','nohup alacritty -e "sleep 2;cmatrix"&;disown'], ["jaro", "abe"])
    #commands = {
    #    "prepareforbattle": ["ssh -p 2222 justin@jforseth.tech export DISPLAY=:0; firefox &", "ssh -p 2222 justin@jforseth.tech export DISPLAY=:0; konsole &", "ssh 192.168.1.3 export DISPLAY=:0 nohup spotify &"],
    #    "lightsout": ['ssh -p 2222 justin@jforseth.tech "xset dpms force off -display :0" & wget 192.168.1.3:5005/?identifier=WDZPQ8P7qXSjRU'],
    #    "turnoutthelights": ['ssh -p 2222 justin@jforseth.tech "xset dpms force off -display :0" & wget 192.168.1.3:5005/?identifier=WDZPQ8P7qXSjRU'],
    #    "hitthelights": ['ssh -p 2222 justin@jforseth.tech "xset dpms force off -display :0" & wget 192.168.1.3:5005/?identifier=WDZPQ8P7qXSjRU'],
    #    "killthelights": ['ssh -p 2222 justin@jforseth.tech "xset dpms force off -display :0" & wget 192.168.1.3:5005/?identifier=WDZPQ8P7qXSjRU'],
    #    "turnthelightsoff": ['ssh -p 2222 justin@jforseth.tech "xset dpms force off -display :0" & wget 192.168.1.3:5005/?identifier=WDZPQ8P7qXSjRU'],
    #    "play": ['ssh -p 2222 justin@jforseth.tech "playerctl play"'],
    #    "resume": ['ssh -p 2222 justin@jforseth.tech "playerctl play"'],
    #    "pause": ['ssh -p 2222 justin@jforseth.tech "playerctl pause"'],
    #    "lightson": ['ssh justin@192.168.1.5 "xset dpms force on -display :0"', 'ssh -p 2222 justin@jforseth.tech "xset dpms force on -display :0"'],
    #    "lock": ['ssh -p 2222 justin@jforseth.tech "loginctl lock-session self"']
    #}
    #actual_commands = commands.get(
    #    command, "echo Invalid Command:{}".format(command))
    #print(actual_commands)
    #for command in actual_commands:
    #    os.system(command)
    return ""


