import os
import os.path
import io
import time
import platform

from flask import *
from werkzeug.utils import secure_filename
from flask_simplelogin import login_required, get_username


from refresh_writer_thumbs import refresh_thumbs

writer = Blueprint("writer", __name__)


@writer.route("/writer")
@login_required()
def writer_home():
    if platform.node() == "backup-server-vm":
        flash(
            "The main jforseth.tech server is currently experiencing issues. Your changes may not be saved when the main server comes back online."
        )
    username = get_username()  # .encode("utf-8")
    path = "userdata/{}/writer/documents/".format(username)
    print(path)
    files = os.listdir(path)
    files = [i for i in files if i != "oopsie"]
    new_files = []
    for i in files:
        if i != "oopsie":
            i = i.replace(".html", "")
            i = i.title()
            new_files.append(i)
    return render_template("writer/writer.html", docs=new_files)


@writer.route("/writer/thumb/<name>")
def writer_thumb(name):
    try:
        return send_file(
            "userdata/{}/writer/thumbnails/{}.html_thumb.png".format(
                get_username(), name.lower()
            )
        )
    except:
        refresh_thumbs(get_username())  # .encode("utf-8"))
        return send_file(
            "userdata/{}/writer/thumbnails/{}.html_thumb.png".format(
                get_username(), name.lower()
            )
        )


@writer.route("/writer/<name>")
@login_required()
def writer_page(name):
    return render_template("writer/Summernote.html", name=name)


@writer.route("/writer/save/<name>", methods=["POST"])
@login_required()
def web_save(name):
    data = request.form.get("editordata")
    return save(name, data)


@writer.route("/writer/document/<name>")
@login_required()
def document_noapi(name):
    return get_document(name)


def save(filename, data):
    filename = filename.lower()
    username = get_username()  # .encode("utf-8")
    path = "userdata/{}/writer/documents/".format(username)
    print(data)
    print(path + "{}.html".format(filename))
    with io.open(path + "{}.html".format(filename), "w", encoding="utf-8") as file:
        file.write(data)
    return redirect("/writer/{}".format(filename))


def get_document(filename):
    filename = filename.lower()
    username = get_username()
    try:
        with io.open(
            "userdata/{}/writer/documents/{}.html".format(username, filename),
            "r",
            encoding="utf-8",
        ) as file:
            document = file.read()
    except IOError:
        io.open(
            "userdata/{}/writer/documents/{}.html".format(
                get_username().encode("utf-8"), filename
            ),
            "w",
        )
        refresh_thumbs(username)
        document = ""
    return Markup(document)
