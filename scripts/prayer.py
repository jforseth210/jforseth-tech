import platform
import random
import pprint  # Useful for debug.
import json
import sqlite3

from flask import *
from flask_simplelogin import login_required, get_username

from simple_mail import send_email
from account_management import (
    generate_token,
    check_token,
    remove_token,
    get_user_from_token,
)
from SensitiveData import *

pp = pprint.PrettyPrinter(indent=4)

prayer = Blueprint("prayer", __name__)
# This is a dictionary that converts the code the user typed in into a parish.
# If adding a new group:
# REMEMBER TO UPDATE THE HTML
PARISH_DICTIONARY = {
    "Public": "Public",
    "STJOHNRE": "Saint John RE",
    "STJOHN": "Saint John Parish",
    "STJOSEPH": "Saint Joseph Parish",
    "STJOSEPHRE": "Saint Joseph RE",
    # 'FFTEACHERS': 'Fairfield Catholic Teachers',
    "JESUS": "LL Small Group 3",
    "MT4H": "State Award Demo",
    # No human should be able to join the testing group:
    TESTING_GROUP_SIGNUP_CODE: "Testing",
}

# Email verification


def get_verification_code():
    with open("text/validcodes.txt", "r") as file:
        VALID_CODES = file.readline()
    random_number = random.randint(0, len(VALID_CODES) - 5)
    return VALID_CODES[random_number: random_number + 5]


def get_verification_email_template():
    with open("text/verification_email_template.html") as file:
        VERIFICATION_EMAIL_TEMPLATE = file.read()
    return VERIFICATION_EMAIL_TEMPLATE


def check_verification_code(code):
    with open("text/validcodes.txt", "r") as file:
        valid_codes = file.readline()

    print("Code:" + code)
    print("Valid Code:" + valid_codes)

    code_validity = code in valid_codes
    print("Code Validity:" + str(code_validity))

    new_code = str(random.randint(10000, 99999))
    print("New Code:" + new_code)
    valid_codes = valid_codes.replace(code, new_code)
    print("New Valid Code List:" + valid_codes)
    with open("text/validcodes.txt", "w") as file:
        file.write(valid_codes)
    return code_validity


def add_to_mailing_list(address, parishes):
    conn = sqlite3.connect("database.db")
    cur = conn.cursor()
    parishstring = "\n".join(parishes)
    if address != "testing@jforseth.tech" and not enter_tests_into_db:
        with conn:
            cur.execute(
                """INSERT INTO users VALUES(:email,:parish)""",
                {"email": address, "parish": parishstring},
            )
    else:
        print(
            "This is the testing email. If you used a normal email, these values would've been added: \nEmail="
            + address
            + "\nParishes="
            + parishes
        )


# Prayer request submissions


def read_prayer_request_template(email, name, prayer_request, parish):
    with open("text/prayer_request_email_template.html") as file:
        PRAYER_REQUEST_TEMPLATE = file.read()

    subject = "{} has sent a prayer request to {}"
    subject = subject.format(name, parish)

    message = PRAYER_REQUEST_TEMPLATE.format(
        email=email, name=name, group=parish, request=prayer_request,
    )

    return (message, subject)


def get_emails_from_parish(parish):
    conn = sqlite3.connect("database.db")
    cur = conn.cursor()

    with conn:
        cur.execute(
            """SELECT prayer_email FROM accounts WHERE prayer_groups LIKE '%{}%'""".format(
                parish
            )
        )
    emails = cur.fetchall()
    emails = [email[0] for email in emails]
    return emails


# The main page


@prayer.route("/prayer")
def prayer_page():
    if platform.node() == "backup-server-vm":
        flash(
            "The jforseth.tech main server is experiencing issues. Emails may fail to send and new accounts may not be saved."
        )
    return render_template("prayer/prayer.html", options=PARISH_DICTIONARY.values())


@prayer.route("/FlaskApp/prayer")
def old_prayer_page():
    return redirect("/prayer")


@prayer.route("/prayer/prayerrequest", methods=["POST", "GET"])
def prayer_request():
    name = escape(request.form.get("name"))
    prequest = escape(request.form.get("prequest"))
    parish = escape(request.form.get("parish"))

    emails = get_emails_from_parish(parish)

    message_template, subject_template = read_prayer_request_template(
        "EMAIL_PLACEHOLDER", name, prequest, parish
    )

    # For testing purposes only, manually overrides email list and sends to my personal account instead:
    # Uncommenting this is a really, really bad idea.
    # emails=[personalemail]
    if current_app.config["TESTING"]:
        print("Looks like this is a test. Here's some data:")
        datadict = {
            "emails": emails,
            "subject_template": subject_template,
            "message_template": message_template,
        }
        return jsonify(datadict)
    flash("Prayer request sent!", category="success")
    return redirect("/prayer")


@prayer.route("/prayer/unsub")
def confirm_unsubscription():
    email = escape(request.args.get("email"))
    group = escape(request.args.get("group"))
    token = generate_token(email, "prayer_unsubscription")
    with open("text/prayer_unsubscription_email_template.html") as file:
        message = file.read()
    message = message.format(email=email, group=group, token=token)
    if group == "ALL":
        group = "all prayer requests."
    flash(
        "Sorry to see you go! Check your email for an unsubscription link.",
        category="success",
    )
    return redirect("/prayer")


@prayer.route("/prayer/unsub_confirmed")
def unsubscribe_page():
    email = escape(request.args.get("email"))
    group = escape(request.args.get("group"))
    token = escape(request.args.get("token"))
    conn = sqlite3.connect("database.db")
    cur = conn.cursor()
    if (
        check_token(token, "prayer_unsubscription")
        and get_user_from_token(token, "prayer_unsubscription") == email
    ):
        with conn:
            cur.execute(
                """SELECT prayer_groups, username FROM accounts WHERE prayer_email='{email}'""".format(
                    email=email
                )
            )
        users = cur.fetchall()
        if group != "ALL":
            for idx, user in enumerate(users):
                user = list(user)
                user[0] = user[0].replace(group, "")
                user[0] = user[0].strip("|")
                user[0] = user[0].replace("||", "|")
                if user[0] == "":
                    users[idx] = ("None", user[1])
                else:
                    users[idx] = (user[0], user[1])
        else:
            users = [("None", user[1]) for user in users]
        with conn:
            for user in users:
                cur.execute(
                    """UPDATE accounts SET prayer_groups = ? WHERE username = ?""",
                    (user[0], user[1]),
                )
        remove_token(token, "prayer_unsubscription")
        flash("Done", category="success")
    else:
        flash("This link is invalid.")
    return redirect("/prayer")


@login_required()
@prayer.route("/prayer/unsubscribe_logged_in")
def unsubscribe_logged_in():
    username = get_username()
    group = escape(request.args.get("group"))
    conn = sqlite3.connect("database.db")
    cur = conn.cursor()
    with conn:
        cur.execute(
            """SELECT prayer_groups FROM accounts WHERE username = ? """, ([
                                                                           username])
        )
    prayer_groups = cur.fetchone()[0]
    prayer_groups = prayer_groups.replace(group, "")
    prayer_groups = prayer_groups.strip("|")
    prayer_groups = prayer_groups.replace("||", "|")
    if prayer_groups == "":
        prayer_groups = "None"
    print(prayer_groups)
    with conn:
        cur.execute(
            """UPDATE accounts SET prayer_groups = ? WHERE username= ?""",
            ([prayer_groups, username]),
        )
    flash("Unsubscribed", category="success")
    return redirect("/account/" + username)


@prayer.route("/prayer/addgroup", methods=["POST"])
def add_group():
    group = escape(request.form.get("group"))
    username = get_username()
    group = PARISH_DICTIONARY.get(group)
    if group is None:
        return redirect("/account/" + username)
    conn = sqlite3.connect("database.db")
    cur = conn.cursor()
    with conn:
        cur.execute(
            """SELECT prayer_groups FROM accounts WHERE username = ? """,
            ([username]),
        )
    prayer_groups = cur.fetchone()[0]
    if prayer_groups == "None":
        prayer_groups = group + "|Public"
        flash("Subscribed", category="success")
    elif group in prayer_groups:
        flash("Already part of this group")
    else:
        prayer_groups = prayer_groups + "|" + group
        flash("Subscribed", category="success")

    with conn:
        cur.execute(
            """UPDATE accounts SET prayer_groups = ? WHERE username= ?""",
            ([prayer_groups, username]),
        )
    return redirect("/account/" + username)
